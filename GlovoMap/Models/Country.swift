//
//  Country.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import ObjectMapper

public class Country: ImmutableMappable {
  public let name: String
  public let code: String
  
  public required init(map: Map) throws {
    name = try map.value("name")
    code = try map.value("code")
  }
}
