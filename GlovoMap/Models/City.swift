//
//  City.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation
import Polyline

public class City: ImmutableMappable {
  public let name: String
  public let code: String
  public let countryCode: String
  public let workingArea: [String]
  public var currency: String?
  public var enabled: Bool?
  public var timeZone: String?
  public var languageCode: String?
  public var busy: Bool?
  
  lazy var workingAreaCoordinates: [CLLocationCoordinate2D] = {
    var coordinates: [CLLocationCoordinate2D] = []
    
    workingArea.forEach { encodedPath in
      if !encodedPath.isEmpty {
        if let coords: [CLLocationCoordinate2D] = decodePolyline(encodedPath) {
          coords.forEach { coord in
            coordinates.append(coord)
          }
        }
      }
    }
    
    return coordinates
  }()
  
  public required init(map: Map) throws {
    name = try map.value("name")
    code = try map.value("code")
    countryCode = try map.value("country_code")
    workingArea = try map.value("working_area")
    
    // Optionals
    currency = try? map.value("currency")
    enabled = try? map.value("enabled")
    timeZone = try? map.value("time_zone")
    languageCode = try? map.value("language_code")
    busy = try? map.value("busy")
  }
}
