//
//  CityProvider.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import Moya

enum LocationProvider : TargetType {
  case countries
  case cities
  case city(code: String)
  
  var baseURL: URL { return URL(string: API.rootUrl)! }
  
  var path: String {
    switch self {
    case .countries:
      return "countries"
    case .cities:
      return "cities"
    case .city(let code):
      return "cities/\(code)"
    }
  }
  var method: Moya.Method {
    switch self {
    case .cities:
      return .get
    case .countries:
      return .get
    case .city(_):
      return .get
    }
  }
  
  var sampleData: Data { return Data() }
  
  var task: Task {
    switch self {
    case .cities:
      return .requestPlain
    case .countries:
      return .requestPlain
    case .city(_):
      return .requestPlain
    }
  }
  
  var headers: [String : String]? {
    return [
      "Content-Type": "application/json"
    ]
  }
}

