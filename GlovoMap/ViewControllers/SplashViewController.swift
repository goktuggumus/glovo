//
//  SplashViewController.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit

class SplashViewController: BaseViewController {
  
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureUI()
    
    CityManager.loadCountriesAndCities()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    CityManager.didLoadCitiesAndCountries()
      .filter { $0 }
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { _ in
        UIApplication.changeRoot(with: .locationPermission)
      })
      .disposed(by: disposeBag)
    
    CityManager.errorOccurred()
      .distinctUntilChanged()
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] errorMessage in
        self?.showAlert(withMessage: errorMessage)
      })
      .disposed(by: disposeBag)
  }
  
  private func configureUI() {
    let bottomSpacing: CGFloat = view.frame.size.height / 4
    
    activityIndicator.snp.makeConstraints { make in
      make.size.equalTo(37)
      make.centerX.equalToSuperview()
      make.bottom.equalToSuperview().offset(-bottomSpacing)
    }
  }
}
