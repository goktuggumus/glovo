//
//  BaseViewController.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import PKHUD

class BaseViewController: UIViewController {
  let disposeBag = DisposeBag()
  
  func showLoadingHUD() {
    HUD.show(.progress)
  }
  
  func hideLoadingHUD() {
    HUD.hide(animated: true)
  }
  
  func showAlert(withMessage message: String, buttonClickHandler: ((UIAlertAction) -> Void)? = nil) {
    let alertViewController = UIAlertController(title: "Info", message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Okay", style: .default, handler: buttonClickHandler)
    alertViewController.addAction(okAction)
    
    present(alertViewController, animated: true, completion: nil)
  }
}
