//
//  LocationPermissionViewController.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class LocationPermissionViewController: BaseViewController {
  @IBOutlet weak var shareLocationButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    (shareLocationButton.rx.tap)
      .asObservable()
      .throttle(2, scheduler: MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] in
        self?.askLocationPermission()
      })
      .disposed(by: disposeBag)
  }

  private func askLocationPermission() {
    handlePermissionStatus()
    LocationPermissionManager.checkPermissionStatus()
  }
  
  private func handlePermissionStatus() {
    LocationPermissionManager.permissionStatus()
      .debug("Permission", trimOutput: true)
      .filter { $0 != .idle }
      .take(1)
      .subscribe(onNext: { [weak self] status in
        switch status {
        case .enabled:
          UIApplication.changeRoot(with: .map)
        case .disabled:
          self?.showChooseLocationPage()
          break
        default:
          break
        }
      })
      .disposed(by: disposeBag)
  }
  
  private func showChooseLocationPage() {
    performSegue(withIdentifier: R.segue.locationPermissionViewController.showChooseLocationPage.identifier, sender: nil)
  }
}


