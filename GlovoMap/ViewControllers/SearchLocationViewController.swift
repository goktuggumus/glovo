//
//  SearchLocationViewController.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchLocationViewController: UITableViewController {
  let disposeBag = DisposeBag()
  
  private var cities: [String: [City]] {
    return CityManager.getCitiesByCountry()
  }
  
  private var countries: [Country] {
    return CityManager.getCountries()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.reloadData()
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  private func getNumberOfRows(in section: Int) -> Int {
    guard let country = getCountry(at: section) else { return 0 }
    
    if let citiesOfCountry = cities[country.code] {
      return citiesOfCountry.count
    }
    
    return 0
  }
  
  private func getCity(at indexPath: IndexPath) -> City? {
    guard let country = getCountry(at: indexPath.section) else { return nil }
    
    if let citiesOfCountry = cities[country.code], citiesOfCountry.count > indexPath.row {
      return citiesOfCountry[indexPath.row]
    }
    
    return nil
  }
  
  private func getCountry(at section: Int) -> Country? {
    guard countries.count > section else { return nil }
    
    return countries[section]
  }
  
  // MARK - Table View
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return getNumberOfRows(in: section)
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return countries.count
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if let country = getCountry(at: section) {
      return country.name
    }
    
    return nil
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell(style: .default, reuseIdentifier: "cityCell")
    
    if let city = getCity(at: indexPath) {
      cell.textLabel?.text = city.name
    }
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let selectedCity = getCity(at: indexPath) {
      UserManager.setSelectedCity(selectedCity)
      UIApplication.changeRoot(with: .map)
    }
  }
}
