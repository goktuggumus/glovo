//
//  MapViewController.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMaps
import RxGoogleMaps

class MapViewController: BaseViewController {
  
  @IBOutlet weak var mapContainer: UIView!
  @IBOutlet weak var mapView: GMSMapView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var codeLabel: UILabel!
  @IBOutlet weak var countryCodeLabel: UILabel!
  @IBOutlet weak var currencyLabel: UILabel!
  @IBOutlet weak var enabledLabel: UILabel!
  @IBOutlet weak var timeZoneLabel: UILabel!
  @IBOutlet weak var languageCodeLabel: UILabel!
  @IBOutlet weak var busyLabel: UILabel!
  @IBOutlet weak var centerMarkerView: UIImageView!
  
  private lazy var cityMapViewModels: [CityMapViewModel] = {
    return CityManager.getCities().map { CityMapViewModel(city: $0) }
  }()
  
  private lazy var mapViewModel: MapViewModel = {
    return MapViewModel(cityMapViewModels: cityMapViewModels)
  }()
  
  private let infoPanelViewModel: InfoPanelViewModel = InfoPanelViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureUI()
    bindViews()
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
  private func configureUI() {
    configureMapView()
  }
  
  private func configureMapView() {
    mapView.rx.idleAt.asObservable()
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] _ in
        self?.refreshCenterMarker()
      })
      .disposed(by: disposeBag)
    
    mapView.rx.handleTapMarker { [weak self] marker in
      self?.handleMarkerTap(with: marker)
      return true
    }
    
    mapView.rx.didChange.asObservable()
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] position in
        self?.handleZoom(zoom: position.zoom)
      })
      .disposed(by: disposeBag)
    
    mapView.rx.myLocation
      .filter { $0 != nil }
      .take(1)
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] location in
        self?.focusCameraToCurrentLocation(currentCoordinate: location!.coordinate)
      })
      .disposed(by: disposeBag)
    
    LocationPermissionManager.permissionStatus()
      .distinctUntilChanged()
      .subscribe(onNext: { [weak self] status in
        let enabled = status == .enabled
        
        self?.mapView.isMyLocationEnabled = enabled
        self?.mapView.settings.myLocationButton = enabled
        
        if !enabled {
          if let selectedCity = UserManager.getSelectedCity() {
            self?.focusCameraToCity(withCityCode: selectedCity.code)
          }
        }
      })
      .disposed(by: disposeBag)
    
    // Add Map Styling
    do {
      // Set the map style by passing the URL of the local file.
      if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
        mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
      } else {
        NSLog("Unable to find mapStyle.json")
      }
    } catch {
      NSLog("One or more of the map styles failed to load. \(error)")
    }
  }
  
  private func bindViews() {
    infoPanelViewModel.name
      .bind(to: nameLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.code
      .bind(to: codeLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.countryCode
      .bind(to: countryCodeLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.currency
      .bind(to: currencyLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.enabled
      .bind(to: enabledLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.timeZone
      .bind(to: timeZoneLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.languageCode
      .bind(to: languageCodeLabel.rx.text)
      .disposed(by: disposeBag)
    
    infoPanelViewModel.busy
      .bind(to: busyLabel.rx.text)
      .disposed(by: disposeBag)
  }
  
  private func addWorkingAreasIfNeeded() {
    guard mapViewModel.currentMapItems != .polygons else { return }
    
    DispatchQueue.main.async { [weak self] in
      guard let `self` = self else { return }
      
      self.mapView.clear()
      
      for cityMapViewModel in self.mapViewModel.cityMapViewModels {
        cityMapViewModel.workingAreaPolygon.map = self.mapView
      }
    }
    
    mapViewModel.currentMapItems = .polygons
  }
  
  private func addMarkersIfNeeded() {
    guard mapViewModel.currentMapItems != .markers else { return }
    
    DispatchQueue.main.async { [weak self] in
      guard let `self` = self else { return }
      
      self.mapView.clear()
      
      for cityMapViewModel in self.mapViewModel.cityMapViewModels {
        cityMapViewModel.marker.map = self.mapView
      }
    }
    
    mapViewModel.currentMapItems = .markers
  }
  
  private func fitCameraToPolygon(_ polygon: GMSPolygon, completion: (()->())? = nil) {
    DispatchQueue.main.async { [weak self] in
      if let path = polygon.path {
        let bounds = GMSCoordinateBounds(path: path)
        let update = GMSCameraUpdate.fit(bounds)
        self?.mapView.moveCamera(update)
        completion?()
      }
    }
  }
  
  private func changeCenterMarkerView(asActive: Bool) {
    centerMarkerView.image = asActive ?
      R.image.centerMapPinActive() :
      R.image.centerMapPinInactive()
  }
  
  private func focusCameraToCity(withCityCode cityCode: String) {
    for cityMapViewModel in mapViewModel.cityMapViewModels {
      if cityMapViewModel.code == cityCode {
        focusCameraToCity(withViewModel: cityMapViewModel)
        break
      }
    }
  }
  
  private func focusCameraToCurrentLocation(currentCoordinate: CLLocationCoordinate2D) {
    let camera = GMSCameraUpdate.setTarget(currentCoordinate, zoom: mapViewModel.currentLocationZoomLevel)
    mapView.moveCamera(camera)
    
    addWorkingAreasIfNeeded()
  }
  
  private func focusCameraToCity(withViewModel cityMapViewModel: CityMapViewModel) {
    addWorkingAreasIfNeeded()
    fitCameraToPolygon(cityMapViewModel.workingAreaPolygon, completion: { [weak self] in
      self?.toggleCenterMarker(show: true)
      self?.infoPanelViewModel.populateData(with: cityMapViewModel.code)
    })
  }
  
  private func handleZoom(zoom: Float) {
    if zoom <= mapViewModel.changeMapItemsZoomLevel {
      addMarkersIfNeeded()
      toggleCenterMarker(show: false)
    } else {
      addWorkingAreasIfNeeded()
      toggleCenterMarker(show: true)
    }
  }
  
  private func toggleCenterMarker(show: Bool) {
    centerMarkerView.isHidden = !show
    refreshCenterMarker()
  }
  
  private func refreshCenterMarker() {
    DispatchQueue.main.async { [weak self] in
      guard let `self` = self else { return }
      
      let centerOfMap = self.mapView.projection.coordinate(for: self.mapView.center)
      var containsLocation: Bool = false
      
      for cityMapViewModel in self.mapViewModel.cityMapViewModels {
        if cityMapViewModel.cityContainsLocation(at: centerOfMap) {
          containsLocation = true
          self.infoPanelViewModel.populateData(with: cityMapViewModel.code)
          break
        }
      }
      
      if !containsLocation {
        self.infoPanelViewModel.cleanData()
      }
      
      self.changeCenterMarkerView(asActive: containsLocation)
    }
  }
  
  private func handleMarkerTap(with marker: GMSMarker) {
    for cityMapViewModel in mapViewModel.cityMapViewModels {
      if cityMapViewModel.marker == marker {
        focusCameraToCity(withViewModel: cityMapViewModel)
        break
      }
    }
  }
}
