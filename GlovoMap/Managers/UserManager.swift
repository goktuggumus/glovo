//
//  UserManager.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 23.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation

public class UserManager {
  static let shared = UserManager()
  
  var selectedCity: City?
}

extension UserManager {
  class func setSelectedCity(_ city: City) {
    shared.selectedCity = city
  }
  
  class func getSelectedCity() -> City? {
    return shared.selectedCity
  }
}
