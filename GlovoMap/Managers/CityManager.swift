//
//  CityManager.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 23.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import RxSwift
import Moya
import ObjectMapper

public class CityManager {
  static let shared: CityManager = CityManager()
  
  private let provider = MoyaProvider<LocationProvider>()
  private let disposeBag = DisposeBag()
  private let didLoadCities: PublishSubject<Bool>
  private let didLoadCountries: PublishSubject<Bool>
  
  fileprivate var countries: [Country] = []
  fileprivate var cities: [City] = []
  fileprivate let didLoadCitiesAndCountries: BehaviorSubject<Bool>
  fileprivate let errorOccurred: PublishSubject<String>
  fileprivate lazy var citiesByCountry: [String: [City]] = {
    var citiesByCountry: [String: [City]] = [:]
    
    cities.forEach { city in
      if citiesByCountry[city.countryCode] == nil {
        citiesByCountry[city.countryCode] = [city]
      } else {
        citiesByCountry[city.countryCode]!.append(city)
      }
    }
    
    return citiesByCountry
  }()
  
  fileprivate lazy var countryNames: [String: String] = {
    return countries.reduce(into: [String: String]()) {
      $0[$1.code] = $1.name
    }
  }()
  
  private init() {
    didLoadCities = PublishSubject()
    didLoadCountries = PublishSubject()
    errorOccurred = PublishSubject()
    didLoadCitiesAndCountries = BehaviorSubject(value: false)
    
    Observable.zip(didLoadCities, didLoadCountries)
      .take(1)
      .subscribe(onNext: { [weak self] _ in
        self?.didLoadCitiesAndCountries.onNext(true)
      })
      .disposed(by: disposeBag)
  }
  
  private func loadCities() {
    provider.rx.request(.cities)
      .debug("Fetch Cities", trimOutput: true)
      .filter(statusCode: 200)
      .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
      .subscribe(onSuccess: { [weak self] response in
        guard let `self` = self else { return }
        
        do {
          if let citiesJSONArray = try response.mapJSON() as? [[String: Any]] {
            let cities = try Mapper<City>().mapArray(JSONArray: citiesJSONArray)
            self.cities = cities
            self.didLoadCities.onNext(true)
          } else {
            self.errorOccurred.onNext("Couldn't parse data")
          }
        } catch {
          self.errorOccurred.onNext("Couldn't parse data")
        }
        
      }, onError: { [weak self] error in
        self?.errorOccurred.onNext(error.localizedDescription)
      })
      .disposed(by: disposeBag)
  }
  
  private func loadCountries() {
    provider.rx.request(.countries)
      .debug("Fetch Countries", trimOutput: true)
      .filter(statusCode: 200)
      .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
      .subscribe(onSuccess: { [weak self] response in
        guard let `self` = self else { return }
        
        do {
          if let countriesJSONArray = try response.mapJSON() as? [[String: Any]] {
            let countries = try Mapper<Country>().mapArray(JSONArray: countriesJSONArray)
            self.countries = countries
            self.didLoadCountries.onNext(true)
          } else {
            self.errorOccurred.onNext("Couldn't parse data")
          }
        } catch {
          self.errorOccurred.onNext("Couldn't parse data")
        }
        
      }, onError: { [weak self] error in
        self?.errorOccurred.onNext(error.localizedDescription)
      })
      .disposed(by: disposeBag)
  }
  
  fileprivate func loadCountriesAndCities() {
    loadCountries()
    loadCities()
  }
}

extension CityManager {
  class func loadCountriesAndCities() {
    shared.loadCountriesAndCities()
  }
  
  class func didLoadCitiesAndCountries() -> Observable<Bool> {
    return shared.didLoadCitiesAndCountries.asObservable()
  }
  
  class func errorOccurred() -> Observable<String> {
    return shared.errorOccurred.asObservable()
  }
  
  class func getCountries() -> [Country] {
    return shared.countries
  }
  
  class func getCitiesByCountry() -> [String: [City]] {
    return shared.citiesByCountry
  }
  
  class func getCities() -> [City] {
    return shared.cities
  }
}
