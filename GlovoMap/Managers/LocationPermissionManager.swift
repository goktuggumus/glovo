//
//  LocationManager.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

public class LocationPermissionManager: NSObject {
  
  static let shared: LocationPermissionManager = LocationPermissionManager()
  
  private lazy var locationManager: CLLocationManager = {
    let locationManager = CLLocationManager()
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    
    return locationManager
  }()
  
  public enum PermissionStatus {
    case enabled
    case disabled
    case idle
  }
  
  private override init() {
    super.init()
  }
  
  fileprivate var permissionStatus: Variable<PermissionStatus> = Variable(.idle)
  
  fileprivate func checkPermissionStatus() {
    locationManager.requestWhenInUseAuthorization()
  }
}

extension LocationPermissionManager: CLLocationManagerDelegate {
  public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    
    switch status {
    case .authorizedAlways, .authorizedWhenInUse:
      permissionStatus.value = .enabled
    case .denied, .restricted:
      permissionStatus.value = .disabled
    default:
      break
    }
  }
}

extension LocationPermissionManager {
  class func checkPermissionStatus() {
    shared.checkPermissionStatus()
  }
  
  class func permissionStatus() -> Observable<PermissionStatus> {
    return shared.permissionStatus.asObservable()
  }
}
