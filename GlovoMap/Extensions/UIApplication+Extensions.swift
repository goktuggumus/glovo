//
//  UIApplication+Extensions.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import UIKit

extension UIApplication {
  
  class func changeRoot(with page: RootPage) {
    guard let window = UIApplication.shared.keyWindow else {
      return
    }
    
    guard let rootViewController = window.rootViewController else {
      return
    }
    
    var newRootViewController: UIViewController?
    
    switch page {
    case .map:
      if let viewController = R.storyboard.map().instantiateInitialViewController() {
        newRootViewController = viewController
      }
    case .locationPermission:
      if let viewController = R.storyboard.userLocation().instantiateInitialViewController() {
        newRootViewController = viewController
      }
    }
    
    if let newRootViewController = newRootViewController {
      newRootViewController.view.frame = rootViewController.view.frame
      newRootViewController.view.layoutIfNeeded()
      
      UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
        window.rootViewController = newRootViewController
      }, completion: nil)
    }
  }
  
  class func openAppSettings() {
    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)!
    UIApplication.shared.open(settingsUrl)
  }
}
