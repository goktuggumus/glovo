//
//  InfoPanelViewModel.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 24.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import ObjectMapper

class InfoPanelViewModel: BaseViewModel {
  public let name: BehaviorSubject<String?>
  public let code: BehaviorSubject<String?>
  public let countryCode: BehaviorSubject<String?>
  public let currency: BehaviorSubject<String?>
  public let enabled: BehaviorSubject<String?>
  public let timeZone: BehaviorSubject<String?>
  public let languageCode: BehaviorSubject<String?>
  public let busy: BehaviorSubject<String?>
  
  private let provider = MoyaProvider<LocationProvider>()
  private var showingCity: String? = nil
  
  override init() {
    self.name = BehaviorSubject(value: nil)
    self.code = BehaviorSubject(value: nil)
    self.countryCode = BehaviorSubject(value: nil)
    self.currency = BehaviorSubject(value: nil)
    self.enabled = BehaviorSubject(value: nil)
    self.timeZone = BehaviorSubject(value: nil)
    self.languageCode = BehaviorSubject(value: nil)
    self.busy = BehaviorSubject(value: nil)
    
    super.init()
  }
  
  public func cleanData() {
    name.onNext(nil)
    code.onNext(nil)
    countryCode.onNext(nil)
    currency.onNext(nil)
    enabled.onNext(nil)
    timeZone.onNext(nil)
    languageCode.onNext(nil)
    busy.onNext(nil)
    showingCity = nil
  }
  
  public func populateData(with cityCode: String) {
    guard showingCity != cityCode else { return }
    
    showingCity = cityCode
    
    provider.rx.request(.city(code: cityCode))
      .filter(statusCode: 200)
      .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
      .subscribe(onSuccess: { [weak self] response in
        guard let `self` = self else { return }
        
        do {
          if let cityJSON = try response.mapJSON() as? [String: Any] {
            let city = try Mapper<City>().map(JSON: cityJSON)
            
            self.name.onNext(city.name)
            self.code.onNext(city.code)
            self.countryCode.onNext(city.countryCode)
            self.currency.onNext(city.currency)
            self.enabled.onNext(city.enabled != nil ? String(describing: city.enabled!) : nil)
            self.timeZone.onNext(city.countryCode)
            self.languageCode.onNext(city.languageCode)
            self.busy.onNext(city.busy != nil ? String(describing: city.busy!) : nil)
          } else {
            self.cleanData()
          }
        } catch {
          self.cleanData()
          NSLog(error.localizedDescription)
        }
      })
      .disposed(by: disposeBag)
  }
}
