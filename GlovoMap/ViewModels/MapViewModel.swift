//
//  MapViewModel.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 24.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation

public class MapViewModel: BaseViewModel {
  
  private enum CameraZoomLevel: Float {
    case world = 1
    case continent = 5
    case city = 10
    case streets = 15
    case buildings = 20
  }
  
  enum MapItem {
    case markers
    case polygons
  }
  
  let cityMapViewModels: [CityMapViewModel]
  var currentMapItems: MapItem? = nil
  
  let currentLocationZoomLevel = CameraZoomLevel.streets.rawValue
  let changeMapItemsZoomLevel = CameraZoomLevel.city.rawValue
  
  init(cityMapViewModels: [CityMapViewModel]) {
    self.cityMapViewModels = cityMapViewModels
    
    super.init()
  }
}
