//
//  CityViewModel.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 24.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation
import GoogleMaps
import RxSwift
import GLKit

class CityMapViewModel {
  private let workingAreaCoordinates: [CLLocationCoordinate2D]
  
  public let code: String
  
  public lazy var workingAreaPolygon: GMSPolygon = {
    let path = GMSMutablePath()
    
    for coordinate in workingAreaCoordinates {
      path.add(coordinate)
    }
    
    let polygon = GMSPolygon(path: path)
    polygon.fillColor = UIColor(red: 0.68, green: 0, blue: 1, alpha: 0.5)
    polygon.strokeWidth = 0
    polygon.geodesic = true
    
    return polygon
  }()
  
  public lazy var cityCoordinate: CLLocationCoordinate2D = {
    // also we can use reverse geocoding instead of these mathematical calculations to get coordinate of city
    
    var x: Float = 0
    var y: Float = 0
    var z: Float = 0
    var coordinatesCount: Float = 0
    
    for coordinate in workingAreaCoordinates {
      let lat = GLKMathDegreesToRadians(Float(coordinate.latitude))
      let long = GLKMathDegreesToRadians(Float(coordinate.longitude))
      
      x += cos(lat) * cos(long)
      y += cos(lat) * sin(long)
      z += sin(lat)
      
      coordinatesCount += 1
    }
    
    x = x / coordinatesCount
    y = y / coordinatesCount
    z = z / coordinatesCount
    
    let resultLong: Float = atan2(y, x)
    let resultHyp: Float = sqrt(x * x + y * y)
    let resultLat: Float = atan2(z, resultHyp)
    let result = CLLocationCoordinate2D(latitude: CLLocationDegrees(GLKMathRadiansToDegrees(resultLat)), longitude: CLLocationDegrees(GLKMathRadiansToDegrees(resultLong)))
    
    return result
  }()
  
  public lazy var marker: GMSMarker = {
    let marker = GMSMarker(position: cityCoordinate)
    
    marker.icon = R.image.workingAreaMapPin()
    marker.isDraggable = false
    
    return marker
  }()
  
  init(city: City) {
    self.code = city.code
    self.workingAreaCoordinates = city.workingAreaCoordinates
  }
  
  public func cityContainsLocation(at point: CLLocationCoordinate2D) -> Bool {
    if let path = workingAreaPolygon.path {
      return GMSGeometryContainsLocation(point, path, true)
    }
    
    return false
  }
}
