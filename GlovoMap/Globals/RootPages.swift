//
//  RootPages.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation

public enum RootPage {
  case map
  case locationPermission
}
