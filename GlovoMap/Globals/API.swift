//
//  API.swift
//  GlovoMap
//
//  Created by Göktuğ Gümüş on 22.07.2018.
//  Copyright © 2018 Goktug Gumus. All rights reserved.
//

import Foundation

struct API {
  static let rootUrl: String = "http://localhost:3000/api/"
}
